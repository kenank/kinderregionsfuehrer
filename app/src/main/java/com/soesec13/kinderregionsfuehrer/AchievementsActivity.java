package com.soesec13.kinderregionsfuehrer;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.soesec13.kinderregionsfuehrer.achievements.Achievement;
import com.soesec13.kinderregionsfuehrer.achievements.AchievementAdapter;
import com.soesec13.kinderregionsfuehrer.achievements.AchievementGetter;
import com.soesec13.kinderregionsfuehrer.fonts.FontEnum;
import com.soesec13.kinderregionsfuehrer.util.AutoFitGridLayoutManager;

import java.util.List;

public class AchievementsActivity extends DrawerActivity {

    private static int numberAchievements;
    private static int numberUnlocked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activityId = 2;
        FrameLayout frameLayout = findViewById(R.id.activity_frame);

        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_achievements, null,false);
        frameLayout.addView(activityView);

        Typeface openSansBold = Typeface.createFromAsset(getAssets(), FontEnum.OPEN_SANS_BOLD.getLocation());
        Typeface openSansRegular = Typeface.createFromAsset(getAssets(), FontEnum.OPEN_SANS_REGULAR.getLocation());

        RecyclerView recyclerView = findViewById(R.id.rv_achievements);
        GridLayoutManager layoutManager
                = new AutoFitGridLayoutManager(this, 600);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        AchievementAdapter achievementAdapter = new AchievementAdapter();
        recyclerView.setAdapter(achievementAdapter);
        recyclerView.setVisibility(View.VISIBLE);
        AchievementGetter achievementGetter = new AchievementGetter(getAssets(), getApplicationContext());

        try {
            List<Achievement> achs = achievementGetter.loadAchievements();
            achievementAdapter.setAchievementData(achs, openSansBold, openSansRegular);
        } catch (Exception e)
        {
            Log.e(AchievementsActivity.class.getSimpleName(), e.toString());
        }

        ProgressBar pb = findViewById(R.id.progressbar_achievements);
        TextView numberAch = findViewById(R.id.number_achievements);
        TextView numberUnl = findViewById(R.id.number_unlocked);
        numberAch.setText(String.valueOf(numberAchievements));
        numberUnl.setText(String.valueOf(numberUnlocked));
        pb.setProgress(numberUnlocked * 100 / numberAchievements);
    }

    public static void setProgressBarData(int numberAchievements, int numberUnlocked)
    {
        AchievementsActivity.numberAchievements = numberAchievements;
        AchievementsActivity.numberUnlocked = numberUnlocked;
    }

}
