package com.soesec13.kinderregionsfuehrer;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebi on 02/02/2018.
 */

public class DrawerActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout = null;
    private ListView mDrawerList = null;
    private RelativeLayout mDrawerContentLayout = null;
    private String[] mDrawerItems;
    private ActionBarDrawerToggle mDrawerToggle = null;
    protected int activityId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_layout);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerList = findViewById(R.id.left_drawer);
        mDrawerItems = getResources().getStringArray(R.array.left_drawer_array);
        mDrawerContentLayout = findViewById(R.id.left_drawer_layout);

        mDrawerList.setAdapter(new Adapter(this));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerContentLayout);

        for (int index = 0; index < menu.size(); index++) {
            MenuItem menuItem = menu.getItem(index);
            if (menuItem != null) {
                // hide the menu items if the drawer is open
                menuItem.setVisible(!drawerOpen);
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            if(position != activityId || activityId == -1)
            {
                switch (position) {
                    case 0: {
                        Intent intent = new Intent(DrawerActivity.this, StartScreenActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 1: {
                        Intent intent = new Intent(DrawerActivity.this, MapViewActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 2: {
                        Intent intent = new Intent(DrawerActivity.this, AchievementsActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 3: {
                        Intent intent = new Intent(DrawerActivity.this, JournalActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case 4:{
                        Intent intent = new Intent(DrawerActivity.this, HelpActivity.class);
                        startActivity(intent);
                        break;
                    }
                    default:
                        break;
                }
            }

            mDrawerLayout.closeDrawer(mDrawerContentLayout);
        }
    }

    private class Adapter extends BaseAdapter{

        private List<DrawerItem> items = new ArrayList<>();
        private Context context;

        public Adapter(Context context)
        {
            this.context = context;
            Resources res = context.getResources();
            items.add(new DrawerItem(0, R.drawable.ic_home, "Hauptmenü"));
            items.add(new DrawerItem(1, R.drawable.ic_map_primary, res.getString(R.string.title_activity_map)));
            items.add(new DrawerItem(2, R.drawable.ic_trophy_primary, res.getString(R.string.title_activity_achievements)));
            items.add(new DrawerItem(3, R.drawable.ic_book_primary, res.getString(R.string.title_activity_journal)));
            items.add(new DrawerItem(4, R.drawable.ic_help_primary, res.getString(R.string.title_activity_help)));
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            for(DrawerItem i: items)
            {
                if(i.getPos() == position)
                {
                    return i;
                }
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.drawer_list_item, parent, false);
            }

            // get current item to be displayed
            DrawerItem currentItem = (DrawerItem) getItem(position);

            // get the TextView for item name and item description
            TextView text = convertView.findViewById(R.id.drawer_item_text);
            ImageView icon = convertView.findViewById(R.id.drawer_item_icon);

            //sets the text for item name and item description from the current item object
            text.setText(currentItem.getText());
            icon.setImageResource(currentItem.getDrawable());

            // returns the view for the current row
            return convertView;
        }

        private class DrawerItem {
            private final int pos;
            private final @DrawableRes int drawable;
            private final String text;

            public DrawerItem(int pos, @DrawableRes int drawable, String text) {
                this.pos = pos;
                this.drawable = drawable;
                this.text = text;
            }

            public int getPos() {
                return pos;
            }

            public int getDrawable() {
                return drawable;
            }

            public String getText() {
                return text;
            }
        }
    }
}