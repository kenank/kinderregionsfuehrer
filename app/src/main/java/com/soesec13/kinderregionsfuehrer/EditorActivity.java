package com.soesec13.kinderregionsfuehrer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.soesec13.kinderregionsfuehrer.journal.JournalEntry;

import java.util.Date;

public class EditorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        Toolbar toolbar = findViewById(R.id.editor_toolbar);
        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_editor_save:
                Intent intent = new Intent();
                intent.putExtra("entry", createEntry());
                setResult(Activity.RESULT_OK, intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private JournalEntry createEntry()
    {
        EditText titleView = findViewById(R.id.editor_title);
        EditText textView = findViewById(R.id.editor_text);
        String title = titleView.getText().toString();
        title = (title.isEmpty()) ? "(Kein Titel)" : title;
        String text = textView.getText().toString();
        text = (text.isEmpty()) ? "(Kein Text)" : text;
        return new JournalEntry(new Date(), title, text);
    }
}
