package com.soesec13.kinderregionsfuehrer;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.soesec13.kinderregionsfuehrer.tutorial.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HelpActivity extends DrawerActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activityId = 4;
        FrameLayout frameLayout = findViewById(R.id.activity_frame);
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_help, null,false);

        frameLayout.addView(activityView);

        expListView = findViewById(R.id.tutorial_expandable_list);
        prepareListData();
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                TextView text = findViewById(R.id.tutorial_text);
                String clickedItem = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);

                int stringid;
                switch(clickedItem)
                {
                    case "Karte":
                        stringid = R.string.help_text_map;
                        break;
                    case "Errungenschaften":
                        stringid = R.string.help_text_achievements;
                        break;
                    case "Logbuch":
                        stringid = R.string.help_text_journal;
                        break;
                    case "Naturpark - Was ist das?":
                        stringid = R.string.help_text_wasistdas;
                        break;
                    case "Welche Naturparks gibt es?":
                        stringid = R.string.help_text_naturparks;
                        break;
                    case "Der Naturpark Südsteiermark":
                        stringid = R.string.help_text_naturpark_suedsteiermark;
                        break;
                    case "Impressum":
                        stringid = R.string.help_text_legal;
                        break;
                    default: stringid = R.string.help_text_default;
                }
                text.setText(stringid);
                return false;
            }
        });
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        listDataHeader.add("Funktionen der App");
        listDataHeader.add("Über den Naturpark");
        listDataHeader.add("Sonstiges");

        List<String> functions = new ArrayList<>();
        functions.add("Karte");
        functions.add("Errungenschaften");
        functions.add("Logbuch");

        List<String> naturpark = new ArrayList<>();
        naturpark.add("Naturpark - Was ist das?");
        naturpark.add("Welche Naturparks gibt es?");
        naturpark.add("Der Naturpark Südsteiermark");

        List<String> sonstiges = new ArrayList<>();
        sonstiges.add("Impressum");

        listDataChild.put(listDataHeader.get(0), functions); // Header, Child data
        listDataChild.put(listDataHeader.get(1), naturpark);
        listDataChild.put(listDataHeader.get(2), sonstiges);
    }
}
