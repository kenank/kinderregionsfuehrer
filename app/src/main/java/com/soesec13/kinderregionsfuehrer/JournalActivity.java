package com.soesec13.kinderregionsfuehrer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.soesec13.kinderregionsfuehrer.fonts.FontEnum;
import com.soesec13.kinderregionsfuehrer.journal.JournalEntry;
import com.soesec13.kinderregionsfuehrer.journal.JournalEntryAdapter;
import com.soesec13.kinderregionsfuehrer.journal.JournalEntryManager;

import java.util.Collections;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

public class JournalActivity extends DrawerActivity {

    private JournalEntryManager manager;
    private static final int SAVE_ENTRY = 1;
    private JournalEntryAdapter adapter;
    private ImageView imageVoid;
    private TextView textVoid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activityId = 3;
        FrameLayout frameLayout = findViewById(R.id.activity_frame);
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_journal, null,false);
        frameLayout.addView(activityView);

        adapter = new JournalEntryAdapter(this);
        imageVoid = findViewById(R.id.image_journal_void);
        textVoid = findViewById(R.id.text_journal_void);

        manager = new JournalEntryManager(getApplicationContext());

        RecyclerView rv = findViewById(R.id.rv_journal);
        rv.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManagerVertical =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManagerVertical);
        rv.setItemAnimator(new SlideInLeftAnimator());

        final Typeface openSansBold = Typeface.createFromAsset(getAssets(), FontEnum.OPEN_SANS_BOLD.getLocation());
        final Typeface openSansRegular = Typeface.createFromAsset(getAssets(), FontEnum.OPEN_SANS_REGULAR.getLocation());

        rv.setAdapter(adapter);


        List<JournalEntry> entries = manager.loadEntries();
        Collections.reverse(entries);
        adapter.setEntriesData(entries, openSansBold, openSansRegular);
        if (entries.size() != 0) {
            hideViews();
        }
    }

    public void onOpenEditor(View view) {
        Intent intent = new Intent(this, EditorActivity.class);
        startActivityForResult(intent, SAVE_ENTRY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SAVE_ENTRY) {
            if (resultCode == RESULT_OK) {
                hideViews();
                JournalEntry entry = (JournalEntry) data.getSerializableExtra("entry");
                adapter.addEntry(entry);
                manager.saveEntry(entry);
            }
        }
    }

    private void hideViews() {
        imageVoid.setVisibility(View.INVISIBLE);
        textVoid.setVisibility(View.INVISIBLE);
    }


}
