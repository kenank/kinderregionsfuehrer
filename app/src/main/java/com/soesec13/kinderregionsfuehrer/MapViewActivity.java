package com.soesec13.kinderregionsfuehrer;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.design.widget.FloatingActionButton;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewManager;
import android.widget.FrameLayout;

import com.soesec13.kinderregionsfuehrer.achievements.Achievement;
import com.soesec13.kinderregionsfuehrer.achievements.AchievementGetter;
import com.soesec13.kinderregionsfuehrer.achievements.AchievementManager;
import com.soesec13.kinderregionsfuehrer.fragments.StationFragment;
import com.soesec13.kinderregionsfuehrer.gmap.GoogleMapsLauncher;
import com.soesec13.kinderregionsfuehrer.gmap.LocationPermission;
import com.soesec13.kinderregionsfuehrer.mapHandling.Map;
import com.soesec13.kinderregionsfuehrer.mapHandling.MapController;
import com.soesec13.kinderregionsfuehrer.mapHandling.MapIdEnum;
import com.soesec13.kinderregionsfuehrer.mapHandling.MapView;
import com.soesec13.kinderregionsfuehrer.popup.ActionItem;
import com.soesec13.kinderregionsfuehrer.popup.QuickAction;
import com.soesec13.kinderregionsfuehrer.station.FileStationGetter;
import com.soesec13.kinderregionsfuehrer.station.IStationDisplayer;
import com.soesec13.kinderregionsfuehrer.station.IStationGetter;
import com.soesec13.kinderregionsfuehrer.station.ITextLoader;
import com.soesec13.kinderregionsfuehrer.station.RawFileTextLoader;
import com.soesec13.kinderregionsfuehrer.station.Station;
import com.soesec13.kinderregionsfuehrer.station.saving.FileStationSaver;
import com.soesec13.kinderregionsfuehrer.station.tracking.StationTracker;
import com.soesec13.kinderregionsfuehrer.util.TouchRecognizer;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MapViewActivity extends DrawerActivity implements IStationDisplayer{
    private static final String STATION_FRAGMENT_TAG = "station_fragment";
    private MapView mapView;
    private MapController mapController;
    private ITextLoader textLoader;
    private StationFragment stationFragment;
    private FragmentManager fragmentManager;
    private TouchRecognizer touchRecognizer;
    private LocationPermission locationPermission;
    private StationTracker stationTracker;
    private QuickAction legend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activityId = 1;
        FrameLayout frameLayout = findViewById(R.id.activity_frame);
        // inflate the custom activity layout
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_map_view, null,false);
        // add the custom layout of this activity to frame layout.
        frameLayout.addView(activityView);

        //setContentView(R.layout.activity_map_view);

        touchRecognizer = new TouchRecognizer(getResources());
        fragmentManager = getFragmentManager();

        mapView = findViewById(R.id.mapview_map);
        mapView.setScale(3);
        IStationGetter fileStationGetter = new FileStationGetter(getAssets());
        mapController = new MapController(mapView, this, fileStationGetter);
        Map defaultMap = new Map(R.drawable.map_new, MapIdEnum.DEFAULT_MAP);
        try {
            mapController.loadMap(defaultMap);
        } catch (IOException e) {
            Log.e(MapViewActivity.class.getSimpleName(), e.getMessage(), e);
        } catch (JSONException e) {
            Log.e(MapViewActivity.class.getSimpleName(), e.getMessage(), e);
        }
        textLoader = new RawFileTextLoader(getResources(), new Html.ImageGetter() {
            @Override
            public Drawable getDrawable(String source) {
                return null;
            }
        });
        locationPermission = new LocationPermission(this);
        locationPermission.checkLocationPermission();


        List<Achievement> achs = new LinkedList<>();
        AchievementGetter achievementGetter = new AchievementGetter(getAssets(), getApplicationContext());
        try {
             achs = achievementGetter.loadAchievements();
        } catch (Exception e)
        {
            Log.e(MapViewActivity.class.getSimpleName(), e.toString());
        }

        Set<Station> stations = new HashSet<>(fileStationGetter.getAllStations());
        Set<Station> visitedStations = new HashSet<>(fileStationGetter
                .loadVisitedStations(getApplicationContext(), stations));

        FileStationSaver saver = new FileStationSaver(this);
        AchievementManager manager = new AchievementManager(visitedStations, stations, achs,  this);

        stationTracker = new StationTracker();
        stationTracker.addVisitor(manager);
        stationTracker.addVisitor(saver);
        stationTracker.addVisitationChecker(manager);

        this.legend = new QuickAction(this);
        ActionItem item1 = new ActionItem(0, getScaled(R.drawable.ic_wasser, 50), "Spaß im und am Wasser");
        ActionItem item2 = new ActionItem(1, getScaled(R.drawable.ic_wandern, 50), "Wandern");
        ActionItem item3 = new ActionItem(2, getScaled(R.drawable.ic_winter, 50), "Winterspaß");
        ActionItem item4 = new ActionItem(3, getScaled(R.drawable.ic_natur, 50), "Wandern");
        ActionItem item5 = new ActionItem(4, getScaled(R.drawable.ic_spielplatz, 50), "Spielplätze und -orte");
        ActionItem item6 = new ActionItem(5, getScaled(R.drawable.ic_radtour, 50), "Radrouten");
        ActionItem item7 = new ActionItem(6, getScaled(R.drawable.ic_wanderroute, 50), "Wanderwege");

        legend.addActionItem(item1);
        legend.addActionItem(item2);
        legend.addActionItem(item3);
        legend.addActionItem(item4);
        legend.addActionItem(item5);
        legend.addActionItem(item6);
        legend.addActionItem(item7);

        if(!locationPermission.isGranted())
        {
            FloatingActionButton fab = findViewById(R.id.fabMap);
            removeButton(fab);
        }
    }

    private Drawable getScaled(@DrawableRes int id, int size)
    {
        Drawable dr = getResources().getDrawable(id);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        return new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, size, size, true));
    }

    @Override
    public void display(Station station) {
        stationFragment = (StationFragment) fragmentManager.findFragmentByTag(STATION_FRAGMENT_TAG);
        if(stationFragment != null)
        {
            fragmentManager.popBackStack();
        }
        stationFragment = (StationFragment) StationFragment.instantiate(this, StationFragment.class.getName());
        stationFragment.initialise(textLoader, station, getAssets(), MapViewActivity.this, stationTracker);
        stationFragment.setLocationPermission(locationPermission);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.animator.slide_to_right, R.animator.slide_to_left,
                                        R.animator.slide_to_right, R.animator.slide_to_left);
        transaction.add(R.id.fragment_container, stationFragment, STATION_FRAGMENT_TAG)
                   .addToBackStack(null);
        transaction.commit();
    }

    public void onShowMyLocation(View v)
    {
        GoogleMapsLauncher launcher = new GoogleMapsLauncher(this);
        try {
            launcher.showMyLocation();
        } catch (GoogleMapsLauncher.MapsNotInstalledException e) {
            new AlertDialog.Builder(this)
                    .setTitle("Upps!")
                    .setMessage("Du hast Google Maps leider nicht installiert\nVersuch es aus dem Playstore zu installieren und probiers dann noch mal!")
                    .create()
                    .show();
        }
    }

    private void removeButton(View button) {
        ((ViewManager) button.getParent()).removeView(button);
    }

    @Override
    public boolean dispatchTouchEvent ( MotionEvent event )
    {
        touchRecognizer.onMotionEvent(event);
        if(event.getAction() == MotionEvent.ACTION_UP && touchRecognizer.isTouchEvent())
        {
            if (stationFragment != null)
            {
                Rect r = new Rect ( 0, 0, 0, 0 );

                int[] windowCoordinates = new int[2];
                stationFragment.getView().getLocationInWindow(windowCoordinates);
                final int adjustedXCoordinate = (int) (event.getRawX() - windowCoordinates[0]);
                final int adjustedYCoordinate = (int) (event.getRawY() - windowCoordinates[1]);
                stationFragment.getView().getHitRect(r);

                boolean intersects = r.contains(adjustedXCoordinate, adjustedYCoordinate);
                if ( !intersects ) {
                    stationFragment = null;
                    fragmentManager.popBackStack();
                    return true;
                }
            }
        }
        return super.dispatchTouchEvent ( event );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_legend, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if(handled)
        {
            return handled;
        }
        if(item.getItemId() == R.id.action_legend)
        {
            onOpenLegend(findViewById(R.id.action_legend));
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        locationPermission.setRequestResult(requestCode, permissions, grantResults);
    }


    public void onOpenLegend(View view) {
        legend.show(view);
    }
}
