package com.soesec13.kinderregionsfuehrer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import com.soesec13.kinderregionsfuehrer.fonts.FontEnum;

public class StartScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

        Typeface regular = Typeface.createFromAsset(getAssets(), FontEnum.OPEN_SANS_REGULAR.getLocation());

        Button showMap = findViewById(R.id.startscreen_open_map_button);
        showMap.setTypeface(regular);
        Button showAchievements = findViewById(R.id.startscreen_open_achievments_button);
        showAchievements.setTypeface(regular);
        Button showJournal = findViewById(R.id.startscreen_open_journal);
        showJournal.setTypeface(regular);
        Button showHelp = findViewById(R.id.startscreen_open_help_button);
        showHelp.setTypeface(regular);

        SharedPreferences settings = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        boolean demo = settings.getBoolean("switch_demo", false);
        ((Switch) findViewById(R.id.switch_demo)).setChecked(demo);
    }

    public void onOpenMap(View button)
    {
        getWindow().setEnterTransition(new Fade());
        getWindow().setExitTransition(new Fade());
        Intent intent = new Intent(this, MapViewActivity.class);
        startActivity(intent);
    }

    public void onOpenAchievements(View button)
    {
        getWindow().setEnterTransition(new Fade());
        getWindow().setExitTransition(new Fade());
        Intent intent = new Intent(this, AchievementsActivity.class);
        startActivity(intent);
    }

    public void onOpenJournal(View button)
    {
        getWindow().setEnterTransition(new Fade());
        getWindow().setExitTransition(new Fade());
        Intent intent = new Intent(this, JournalActivity.class);
        startActivity(intent);
    }

    public void onOpenHelp(View view) {
        getWindow().setEnterTransition(new Fade());
        getWindow().setExitTransition(new Fade());
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    public void onSwitch(View view) {
        SharedPreferences settings = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        if(((Switch)view.findViewById(R.id.switch_demo)).isChecked()) {
            editor.putBoolean("switch_demo", true);
            editor.apply();
            Snackbar mySnackbar = Snackbar.make(view,
                    "Stationen werden nun ohne Prüfung des Standorts freigeschalten!", Snackbar.LENGTH_SHORT);
            mySnackbar.show();
        } else {
            editor.putBoolean("switch_demo", false);
            editor.commit();
        }
    }
}
