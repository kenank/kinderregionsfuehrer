package com.soesec13.kinderregionsfuehrer.achievements;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONObject;

import java.io.InputStream;

public class Achievement {


    private final Bitmap image;
    private final Bitmap lockedImage;
    private final String title;
    private final String description;
    private boolean unlocked;
    private final Requirement requirement;

    private Achievement(Bitmap image, Bitmap lockedImage, String title, String description, String type, String requirment) {
        this.image = image;
        this.lockedImage = lockedImage;
        this.title = title;
        this.description = description;
        this.requirement = new Requirement(type, requirment);
    }

    public Bitmap getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Bitmap getLockedImage() {
        return lockedImage;
    }

    public boolean isUnlocked() {
        return unlocked;
    }

    public void setUnlocked(boolean unlocked) {
        this.unlocked = unlocked;
    }

    public static Achievement parseJsonObject(JSONObject object, AssetManager assets) throws Exception {
        String assetId = object.getString("assetId");
        String title = object.getString("title");
        String description= object.getString("description");
        String type = object.getString("type");
        String req = object.getString("req");
        InputStream inputStream = assets.open("images/" + assetId);
        InputStream inputStreamForLocked = assets.open("images/" + "locked_" + assetId);

        return new Achievement(BitmapFactory.decodeStream(inputStream),
                BitmapFactory.decodeStream(inputStreamForLocked), title, description, type, req);
    }

    public Requirement getRequirement() {
        return requirement;
    }
}
