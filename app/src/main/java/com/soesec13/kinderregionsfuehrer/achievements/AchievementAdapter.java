package com.soesec13.kinderregionsfuehrer.achievements;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.soesec13.kinderregionsfuehrer.R;

import java.util.List;

public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.AchievementAdapterViewHolder>{

    private List<Achievement> achievements;
    private Typeface bold;
    private Typeface regular;

    public AchievementAdapter() {

    }
    
    public class AchievementAdapterViewHolder extends RecyclerView.ViewHolder {

        public final ImageView achievementImageView;
        public final TextView achievementTitle;
        public final TextView achievementDescription;
        public final TextView achievementStatus;

        public AchievementAdapterViewHolder(View view) {
            super(view);
            achievementImageView = view.findViewById(R.id.ach_item);
            achievementTitle = view.findViewById(R.id.ach_title);
            achievementDescription = view.findViewById(R.id.ach_description);
            achievementStatus = view.findViewById(R.id.ach_status);
        }
    }
    
    @Override
    public AchievementAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.achievements_item;
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForListItem, viewGroup, false);
        return new AchievementAdapterViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(AchievementAdapterViewHolder achievementAdapterViewHolder, int position) {
        Achievement ach = achievements.get(position);
        achievementAdapterViewHolder.achievementImageView.setImageBitmap(ach.getImage());
        if(!ach.isUnlocked())
        {
            achievementAdapterViewHolder.achievementImageView.setImageBitmap(ach.getLockedImage());
        } else {
            achievementAdapterViewHolder.achievementStatus.setText(R.string.unlocked);
            achievementAdapterViewHolder.achievementImageView.setImageBitmap(ach.getImage());
        }
        achievementAdapterViewHolder.achievementTitle.setTypeface(bold);
        achievementAdapterViewHolder.achievementTitle.setText(ach.getTitle());
        achievementAdapterViewHolder.achievementDescription.setTypeface(regular);
        achievementAdapterViewHolder.achievementDescription.setText(ach.getDescription());
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    
    @Override
    public int getItemCount() {
        if (null == achievements) return 0;
        return achievements.size();
    }
    
    public void setAchievementData(List<Achievement> imageData, Typeface bold, Typeface regular) {
        achievements = imageData;
        this.bold = bold;
        this.regular = regular;
        notifyDataSetChanged();
    }
}
