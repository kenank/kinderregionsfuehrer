package com.soesec13.kinderregionsfuehrer.achievements;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.soesec13.kinderregionsfuehrer.AchievementsActivity;
import com.soesec13.kinderregionsfuehrer.util.IOUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AchievementGetter {

    private final AssetManager assets;
    private final Context context;
    private Set<String> unlocked;
    private final String FILE_ACHIEVEMENTS = "achievements/achievements.json";

    public AchievementGetter(AssetManager assets, Context context) {
        this.assets = assets;
        this.context = context;
    }

    private List<Achievement> loadFromJson(JSONObject object) throws JSONException {
        List<Achievement> achievementList = new ArrayList<>();
        JSONArray achievementArray = object.getJSONArray("Achievements");
        unlocked = getUnlocked();
        AchievementsActivity.setProgressBarData(achievementArray.length(), unlocked.size());

        for (int i = 0; i < achievementArray.length(); i++) {
            JSONObject jsonAchievement = achievementArray.getJSONObject(i);
            try {
                Achievement parsedAchievement = Achievement.parseJsonObject(jsonAchievement, assets);
                parsedAchievement.setUnlocked(isUnlocked(parsedAchievement.getTitle()));
                achievementList.add(parsedAchievement);
            } catch (Exception ex) {
                Log.e(AchievementGetter.class.getSimpleName(), ex.toString());
            }
        }
        return achievementList;
    }

    public List<Achievement> loadAchievements() throws IOException, JSONException {
        InputStream jsonInput;
        jsonInput = assets.open(FILE_ACHIEVEMENTS);

        BufferedReader reader = new BufferedReader(new InputStreamReader(jsonInput));
        StringBuilder json = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            json.append(line);
        }
        JSONObject achievements = new JSONObject(json.toString());
        return loadFromJson(achievements);
    }

    private boolean isUnlocked(String title) {
        return unlocked.contains(title);
    }

    private Set<String> getUnlocked() {
        Set<String> unlockedAchievements = new HashSet<>();
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            File file = new File(context.getFilesDir() + "/" + AchievementManager.FILE_UNLOCKED_ACHIEVEMENTS);
            if (file.exists()) {
                fis = new FileInputStream(file);
                isr = new InputStreamReader(fis);
                br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    if(!line.isEmpty()) {
                        unlockedAchievements.add(line);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(AchievementGetter.class.getSimpleName(), e.toString());
        } finally {
            IOUtil.closeQuietly(fis, isr, br);
        }
        return unlockedAchievements;
    }

}
