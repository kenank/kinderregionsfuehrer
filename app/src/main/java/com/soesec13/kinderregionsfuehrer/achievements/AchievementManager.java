package com.soesec13.kinderregionsfuehrer.achievements;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;

import com.soesec13.kinderregionsfuehrer.station.Station;
import com.soesec13.kinderregionsfuehrer.station.tracking.IStationVisitor;
import com.soesec13.kinderregionsfuehrer.station.tracking.IVisitationChecker;
import com.soesec13.kinderregionsfuehrer.util.IOUtil;

import java.io.FileOutputStream;
import java.util.List;
import java.util.Set;

public class AchievementManager implements IStationVisitor, IVisitationChecker {
    private final Set<Station> visited;
    private final List<Achievement> achievements;
    private final Context context;
    public static final String FILE_UNLOCKED_ACHIEVEMENTS = "unlocked.txt";

    public AchievementManager(Set<Station> visited, Set<Station> stations, List<Achievement> achievements,  Context context) {
        this.visited = visited;
        this.achievements = achievements;
        for (Achievement achievement : achievements) {
            Requirement req = achievement.getRequirement();
            req.setRequiredCount(stations);
        }
        this.context = context;
    }

    private void updateAchievements() {
        for (Achievement ach : achievements) {
            if (ach.isUnlocked()) {
                continue;
            }
            Requirement req = ach.getRequirement();
            if (req.isSatisfied(visited)) {
                this.unlockAchievement(ach);
            }
        }
    }

    private void unlockAchievement(Achievement achievement) {
        AchievementManager.showUnlockMessage(context, achievement.getTitle());
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(context.getFilesDir() + "/" + FILE_UNLOCKED_ACHIEVEMENTS, true);
            outputStream.write(("\n" + achievement.getTitle()).getBytes());
            outputStream.close();
        } catch (Exception e) {
            Log.e("AchievementManager", "Unlocked achievement could not be saved:\n" + e.toString());
        } finally {
            IOUtil.closeQuietly(outputStream);
        }
    }

    private static void showUnlockMessage(Context context, String achievement){
        new AlertDialog.Builder(context)
                .setTitle("Super!")
                .setMessage("Du hast die Errungenschaft \""+achievement+"\" freigeschalten!")
                .create()
                .show();
    }

    @Override
    public void visitStation(Station station) {
        visited.add(station);
        updateAchievements();
    }

    @Override
    public boolean checkIsVisited(Station station) {
        return visited.contains(station);
    }


}
