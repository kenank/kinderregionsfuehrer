package com.soesec13.kinderregionsfuehrer.achievements;

import com.soesec13.kinderregionsfuehrer.station.Station;

import java.util.Set;


public class Requirement {
    private final String type;
    private int required;
    private final String requirement;
    private static final String ALL = "All";
    private static final String GENERAL = "General";

    public Requirement(String type, String requirement) {
        this.type = type;
        this.requirement = requirement;
    }

    public void setRequiredCount(Set<Station> stations)
    {
        if(requirement.equals(ALL))
        {
            this.required = Requirement.countType(type, stations);
        }
        else
        {
            this.required = Integer.parseInt(requirement);
        }
    }

    public String getType() {
        return type;
    }

    public boolean isSatisfied(Set<Station> stations)
    {
        int count = Requirement.countType(type, stations);
        return required == count;
    }

    public static int countType(String type, Set<Station> stations)
    {
        if(type.equals(GENERAL))
        {
            return stations.size();
        }
        int counter = 0;
        for(Station s: stations)
        {
            if(type.equals(GENERAL) || s.getType().equals(type))
            {
                counter++;
            }
        }
        return counter;
    }
}
