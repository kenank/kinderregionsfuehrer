package com.soesec13.kinderregionsfuehrer.fonts;

public enum FontEnum {
    COMIC_ZINE("fonts/comic_zine_ot.ttf"),
    OPEN_SANS_REGULAR("fonts/OpenSans-Regular.ttf"),
    OPEN_SANS_BOLD("fonts/OpenSans-Bold.ttf"),
    OPEN_SANS_ITALIC("fonts/OpenSans-Italic.ttf");

    private final String location;

    FontEnum(String s) {
        location = s;
    }

    public String getLocation() {
        return location;
    }
}
