package com.soesec13.kinderregionsfuehrer.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.soesec13.kinderregionsfuehrer.R;
import com.soesec13.kinderregionsfuehrer.fonts.FontEnum;
import com.soesec13.kinderregionsfuehrer.gmap.GoogleMapsLauncher;
import com.soesec13.kinderregionsfuehrer.gmap.LocationLoader;
import com.soesec13.kinderregionsfuehrer.gmap.LocationPermission;
import com.soesec13.kinderregionsfuehrer.station.ITextLoader;
import com.soesec13.kinderregionsfuehrer.station.Station;
import com.soesec13.kinderregionsfuehrer.station.tracking.StationTracker;
import com.soesec13.kinderregionsfuehrer.util.DialogUtil;
import com.soesec13.kinderregionsfuehrer.util.DistanceCalculator;

public class StationFragment extends Fragment implements View.OnClickListener{
    private ITextLoader textLoader;
    private Station station;
    private Typeface headlineFont;
    private Typeface textFont;
    private GoogleMapsLauncher launcher;
    private Button gmapButton;
    private Button visitButton;
    private LocationPermission permission;
    private Context baseContext;
    private StationTracker stationTracker;

    public void initialise(ITextLoader textLoader, Station station, AssetManager assetManager, Context context, StationTracker stationTracker) {
        this.textLoader = textLoader;
        this.station = station;
        baseContext = context;
        headlineFont = Typeface.createFromAsset(assetManager, FontEnum.OPEN_SANS_BOLD.getLocation());
        textFont = Typeface.createFromAsset(assetManager, "fonts/OpenSans-Regular.ttf");
        this.stationTracker = stationTracker;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_station, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        launcher = new GoogleMapsLauncher(this.getActivity());
        LinearLayout layout = view.findViewById(R.id.station_fragment_layout);

        final TextView name = view.findViewById(R.id.station_fragment_name);
        final Activity act = getActivity();
        TextView text = view.findViewById(R.id.station_fragment_text);
        ImageView icon = view.findViewById(R.id.fragment_icon);

        switch (station.getType()) {
            case "Naturerlebnis":
                icon.setImageDrawable(baseContext.getDrawable(R.drawable.ic_natur));
                break;
            case "Wasser":
                icon.setImageDrawable(baseContext.getDrawable(R.drawable.ic_wasser));
                break;
            case "Winterspass":
                icon.setImageDrawable(baseContext.getDrawable(R.drawable.ic_winter));
                break;
            case "Wandern":
                icon.setImageDrawable(baseContext.getDrawable(R.drawable.ic_wandern));
                break;
            case "Spielplatz":
                icon.setImageDrawable(baseContext.getDrawable(R.drawable.ic_spielplatz));
                break;
            default:
                ((ViewManager) icon.getParent()).removeView(icon);
        }
        gmapButton = layout.findViewById(R.id.gmaps_button);
        visitButton = layout.findViewById(R.id.unlock_button);

        gmapButton.setOnClickListener(this);
        visitButton.setOnClickListener(this);

        final Spanned stationText = textLoader.loadTextOfStation(station);

        name.setText(station.getName());
        text.setText(stationText);
        name.setTypeface(headlineFont);
        text.setTypeface(textFont);

        if(!station.hasGeoUri())
        {
            removeButton(gmapButton);
        }
        if(!permission.isGranted() || stationTracker.isStationVisited(station))
        {
            removeButton(visitButton);
        }
    }

    private void removeButton(Button button) {
        ((ViewManager) button.getParent()).removeView(button);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(gmapButton))
        {
            this.onOpenGmap();
        }
        else if(v.equals(visitButton))
        {
            SharedPreferences settings = getActivity().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            boolean demo = settings.getBoolean("switch_demo", false);
            if(demo)
            {
                notifySuccessfulVisit();
            } else {
                this.onVisitStation();
            }
        }
    }

    private void onOpenGmap()
    {
        try {
            launcher.show(station);
        } catch (GoogleMapsLauncher.MapsNotInstalledException e) {
            DialogUtil.displayWarning(getActivity(), "Error", e.getMessage());
        }
    }

    private void onVisitStation()
    {
        LocationLoader locationLoader = permission.getLoader();
        Location currentLocation = locationLoader.getBestLocation();
        if(currentLocation == null)
        {
            this.showLocationNotFound();
            return;
        }
        double lat1 = currentLocation.getLatitude();
        double lon1 = currentLocation.getLongitude();
        double lat2 = station.getLat();
        double lon2 = station.getLng();
        double distanceInMeters = DistanceCalculator.distance(lat1, lon1, lat2, lon2);

        if(distanceInMeters <= Station.VISIT_THRESHOLD)
        {
            notifySuccessfulVisit();
        }
        else
        {
            Snackbar mySnackbar = Snackbar.make(getActivity().findViewById(R.id.mapview_map),
                    String.format("Du bist leider %.2f Meter zu weit weg, probier etwas näher zu der Station zu gehen.", distanceInMeters), Snackbar.LENGTH_LONG);
            mySnackbar.show();
        }
    }

    private void notifySuccessfulVisit()
    {
        stationTracker.notifyVisit(station);
        this.removeButton(visitButton);
        Snackbar mySnackbar = Snackbar.make(getActivity().findViewById(R.id.mapview_map),
                "Du hast die Station erfolgreich als besucht markiert!", Snackbar.LENGTH_SHORT);
        mySnackbar.show();
    }

    private void showLocationNotFound()
    {
        Log.d(this.getClass().getSimpleName(), "Location not Found");
        Snackbar mySnackbar = Snackbar.make(getActivity().findViewById(R.id.mapview_map),
                "Wir konnten deinen Standort leider nicht finden.Versuchs später noch mal!", Snackbar.LENGTH_LONG);
        mySnackbar.show();
    }


    public void setLocationPermission(LocationPermission locationPermission) {
        this.permission = locationPermission;
    }

}
