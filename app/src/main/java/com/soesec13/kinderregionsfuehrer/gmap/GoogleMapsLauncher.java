package com.soesec13.kinderregionsfuehrer.gmap;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.soesec13.kinderregionsfuehrer.station.Station;

public class GoogleMapsLauncher {
    private final Activity activity;

    public GoogleMapsLauncher(Activity activity) {
        this.activity = activity;
    }

    public void show(Station station) throws MapsNotInstalledException {
        Uri gmUri = Uri.parse(station.getGMapUri());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        this.open(mapIntent);
    }

    public void showMyLocation() throws MapsNotInstalledException {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW);
        mapIntent.setPackage("com.google.android.apps.maps");
        this.open(mapIntent);
    }

    private void open(Intent mapIntent) throws MapsNotInstalledException
    {
        if(mapIntent.resolveActivity(activity.getPackageManager()) != null)
        {
            activity.startActivity(mapIntent);
        }
        else
        {
            throw new MapsNotInstalledException();
        }
    }

    public class MapsNotInstalledException extends Exception{
        public MapsNotInstalledException() {
            super("Google Maps is not installed on this device");
        }
    }
}
