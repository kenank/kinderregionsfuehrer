package com.soesec13.kinderregionsfuehrer.gmap;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

public class LocationLoader {
    private final LocationManager manager;
    private final Context context;

    public LocationLoader(Context context) {
        manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.context = context;

    }

    public Location getBestLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            throw new RuntimeException("Permission not granted");
        }
        Location locationGPS = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) {
            GPSLocationTime = locationGPS.getTime();
        }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if (0 < GPSLocationTime - NetLocationTime) {
            return locationGPS;
        } else {
            return locationNet;
        }
    }

}

