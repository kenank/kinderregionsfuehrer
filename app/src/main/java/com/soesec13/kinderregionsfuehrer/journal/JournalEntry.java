package com.soesec13.kinderregionsfuehrer.journal;

import java.io.Serializable;
import java.util.Date;

public class JournalEntry implements Serializable{

    private Date date;
    private String title;
    private String text;

    public JournalEntry(Date date, String title, String text) {
        this.date = date;
        this.title = title;
        this.text = text;
    }

    public JournalEntry() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JournalEntry)) return false;

        JournalEntry that = (JournalEntry) o;

        if (!date.toString().equals(that.date.toString())) return false;
        return title.equals(that.title) && text.equals(that.text);
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + text.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "JournalEntry{" +
                "date=" + date.toString() +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
