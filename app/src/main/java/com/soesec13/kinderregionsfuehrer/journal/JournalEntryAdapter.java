package com.soesec13.kinderregionsfuehrer.journal;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.soesec13.kinderregionsfuehrer.R;

import java.text.DateFormat;
import java.util.List;

public class JournalEntryAdapter extends RecyclerView.Adapter<JournalEntryAdapter.JournalEntryAdapterViewHolder> {

    private List<JournalEntry> entries;
    private Typeface bold;
    private Typeface regular;
    private Context context;
    private final DateFormat df = DateFormat.getDateInstance();

    public JournalEntryAdapter(Context context) {
        this.context = context;
    }

    @Override
    public JournalEntryAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.journal_item, parent, false);
        return new JournalEntryAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JournalEntryAdapterViewHolder holder, int position) {
        final JournalEntry j = entries.get(position);
        final int adapterPosition = holder.getAdapterPosition();
        holder.date.setTypeface(regular);
        holder.date.setText(df.format(j.getDate()));
        holder.title.setTypeface(bold);
        holder.title.setText(j.getTitle());
        holder.text.setTypeface(regular);
        holder.text.setText(j.getText());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = deleteDialog(adapterPosition, j);
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (entries == null) ? 0 : entries.size();
    }

    public void setEntriesData(List<JournalEntry> entries, Typeface bold, Typeface regular) {
        this.entries = entries;
        this.bold = bold;
        this.regular = regular;
        notifyDataSetChanged();
    }

    public void addEntry(JournalEntry entry) {
        entries.add(0, entry);
        notifyDataSetChanged();
    }

    private void removeAt(int position) {
        entries.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, entries.size());
    }

    private AlertDialog deleteDialog(final int position, final JournalEntry entry) {
        return new AlertDialog.Builder(
                new ContextThemeWrapper(context, R.style.DeleteDialog))
                //set message, title, and icon
                .setTitle("Löschen")
                .setMessage("Möchtest du diesen Eintrag wirklich löschen?")
                .setIcon(R.drawable.ic_delete)
                .setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        removeAt(position);
                        JournalEntryManager manager = new JournalEntryManager(context);
                        manager.deleteEntry(entry);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
    }

    class JournalEntryAdapterViewHolder extends ViewHolder {

        final TextView date;
        final TextView title;
        final TextView text;
        final ImageView delete;

        JournalEntryAdapterViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.journal_item_date);
            title = itemView.findViewById(R.id.journal_item_title);
            text = itemView.findViewById(R.id.journal_item_text);
            delete = itemView.findViewById(R.id.delete_entry_button);
        }
    }


}
