package com.soesec13.kinderregionsfuehrer.journal;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.soesec13.kinderregionsfuehrer.util.IOUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class JournalEntryManager {

    private final static String FILE_JOURNAL = "journal_entries.json";
    private Context context;

    public JournalEntryManager(Context context) {
        this.context = context;
    }

    public void saveEntry(JournalEntry entry) {

        PrintWriter pw = null;
        OutputStream os = null;
        try {
            List<JournalEntry> existingEntries = loadEntries();
            existingEntries.add(entry);

            os = context.openFileOutput(FILE_JOURNAL, Context.MODE_PRIVATE);
            pw = new PrintWriter(os);

            writeEntryList(existingEntries, pw);
        } catch (Exception e) {
            Log.e(JournalEntryManager.class.getSimpleName(), e.toString());
        } finally {
            IOUtil.closeQuietly(pw, os);
        }
    }

    public void deleteEntry(JournalEntry entryToBeRemoved) {
        PrintWriter pw = null;
        OutputStream os = null;
        try {
            List<JournalEntry> existingEntries = loadEntries();

            os = context.openFileOutput(FILE_JOURNAL, Context.MODE_PRIVATE);
            pw = new PrintWriter(os);

            existingEntries.remove(entryToBeRemoved);
            writeEntryList(existingEntries, pw);
        } catch (Exception e) {
            Log.e(JournalEntryManager.class.getSimpleName(), e.toString());
        } finally {
            IOUtil.closeQuietly(pw, os);
        }
    }

    private void writeEntryList(List<JournalEntry> entries, PrintWriter pw) {
        Gson gson = new Gson();
        pw.print("");
        if (!entries.isEmpty()) {
            pw.write(gson.toJson(entries));
        }
    }

    public List<JournalEntry> loadEntries() {
        File journal = new File(context.getFilesDir() + "/" + FILE_JOURNAL);

        BufferedReader reader = null;
        InputStreamReader isr = null;
        FileInputStream fis = null;
        List<JournalEntry> response = null;
        try {
            journal.createNewFile();
            fis = new FileInputStream(journal);
            isr = new InputStreamReader(fis);
            reader = new BufferedReader(isr);

            Gson gson = new Gson();
            response = gson.fromJson(reader,
                    new TypeToken<LinkedList<JournalEntry>>(){}.getType());
        } catch (Exception e) {
            Log.e(JournalEntryManager.class.getSimpleName(), e.toString());
        } finally {
            IOUtil.closeQuietly(fis, isr, reader);
        }
        return (response != null) ? response : new LinkedList<JournalEntry>();
    }


}
