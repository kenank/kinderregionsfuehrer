package com.soesec13.kinderregionsfuehrer.mapHandling;

public final class Map {
    private final int resourceId;
    private final MapIdEnum mapId;

    public Map(int resourceId, MapIdEnum map) {
        this.resourceId = resourceId;
        this.mapId = map;
    }

    public MapIdEnum getMapId() {
        return mapId;
    }

    public int getResourceId() {
        return resourceId;
    }
}
