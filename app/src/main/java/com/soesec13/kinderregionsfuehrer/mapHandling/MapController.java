package com.soesec13.kinderregionsfuehrer.mapHandling;

import android.widget.ImageView;

import com.github.chrisbanes.photoview.OnPhotoTapListener;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.soesec13.kinderregionsfuehrer.station.IStationDisplayer;
import com.soesec13.kinderregionsfuehrer.station.IStationGetter;
import com.soesec13.kinderregionsfuehrer.station.Station;

import org.json.JSONException;

import java.io.IOException;

public class MapController implements OnPhotoTapListener{
    private final MapView mapView;
    private final IStationGetter stationGetter;
    private final IStationDisplayer stationDisplayer;

    public MapController(MapView mapView, IStationDisplayer stationDisplayer, IStationGetter stationGetter) {
        this.mapView = mapView;
        this.stationDisplayer = stationDisplayer;
        this.stationGetter = stationGetter;
        PhotoViewAttacher attacher = new PhotoViewAttacher(mapView);
        attacher.setOnPhotoTapListener(this);
    }

    public void loadMap(Map map) throws IOException, JSONException {
        mapView.setMap(map);
        stationGetter.loadStationsFrom(map);
    }

    @Override
    public void onPhotoTap(ImageView view, float x, float y) {
        if(!stationGetter.hasStationInLocation(x,y))
        {
            return;
        }
        Station tappedStation = stationGetter.getStation(x,y);
        stationDisplayer.display(tappedStation);
    }
}
