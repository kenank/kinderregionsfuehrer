package com.soesec13.kinderregionsfuehrer.mapHandling;

import android.content.Context;
import android.util.AttributeSet;

import com.github.chrisbanes.photoview.PhotoView;

public class MapView extends PhotoView {
    public MapView(Context context) {
        super(context);
    }

    public MapView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public MapView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
    }

    public MapView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setMap(Map map)
    {
        this.setImageResource(map.getResourceId());
    }
}
