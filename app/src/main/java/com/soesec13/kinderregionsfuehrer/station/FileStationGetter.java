package com.soesec13.kinderregionsfuehrer.station;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.soesec13.kinderregionsfuehrer.mapHandling.Map;
import com.soesec13.kinderregionsfuehrer.mapHandling.MapIdEnum;
import com.soesec13.kinderregionsfuehrer.station.saving.FileStationSaver;
import com.soesec13.kinderregionsfuehrer.util.IOUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FileStationGetter implements IStationGetter {
    private final List<Station> stations = new ArrayList<>();
    private int mapWidthInPixels;
    private int mapHeightInPixels;
    private Station lastSelectedStation = null;
    private final AssetManager assets;

    public FileStationGetter(AssetManager assets) {
        this.assets = assets;
    }

    private void loadFromJson(JSONObject object) throws JSONException {
        stations.clear();
        mapWidthInPixels = object.getInt("MapWidth");
        mapHeightInPixels = object.getInt("MapHeight");
        JSONArray stationArray = object.getJSONArray("Stations");
        for (int i = 0; i < stationArray.length(); i++) {
            JSONObject jsonStation = stationArray.getJSONObject(i);
            try{
                Station parsedStation = Station.parseJsonObject(jsonStation);
                stations.add(parsedStation);
            }catch (Exception ex)
            {
                Log.w(FileStationGetter.class.getSimpleName(), new Exception("Failed to parse station", ex));
            }
        }
    }


    @Override
    public void loadStationsFrom(Map map) throws IOException, JSONException {
        MapIdEnum id = map.getMapId();
        InputStream jsonInput;
        switch (id)
        {
            case DEFAULT_MAP:
                jsonInput = assets.open("stations/stations.json");
                break;
            default:
                throw new IOException("Unknown Map! Failed to load stations of " + id.name());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(jsonInput));
        StringBuilder json = new StringBuilder();
        String line;
        while((line = reader.readLine())!= null)
        {
            json.append(line);
        }
        JSONObject stations = new JSONObject(json.toString());
        this.loadFromJson(stations);
    }

    //TODO Check if working properly
    @Override
    public boolean hasStationInLocation(double percentageX, double percentageY) {
        int pixelX = (int) (mapWidthInPixels * percentageX);
        int pixelY = (int) (mapHeightInPixels * percentageY);
        for (Station station: stations) {
            if(station.isClicked(pixelX, pixelY))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public Station getStation(double percentageX, double percentageY) {
        int pixelX = (int) (mapWidthInPixels * percentageX);
        int pixelY = (int) (mapHeightInPixels * percentageY);

        boolean isHit = false;
        for (Station station: stations) {
            if(station.isClicked(pixelX, pixelY))
            {
                if(lastSelectedStation == station)
                {
                    isHit = true;
                    continue;
                }
                lastSelectedStation = station;
                return station;
            }
        }
        if(isHit)
        {
            return lastSelectedStation;
        }
        return null;
    }

    @Override
    public List<Station> getAllStations() {
        return stations;
    }

    @Override
    public Set<Station> loadVisitedStations(Context context, Set<Station> stations) {
        Set<Station> visitedStations = new HashSet<>();
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            File file = new File(context.getFilesDir() + "/" + FileStationSaver.FILE_STATIONS_VISITED);
            if (file.exists()) {
                fis = new FileInputStream(file);
                isr = new InputStreamReader(fis);
                br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    for (Station station : stations) {
                        if (line.equals(station.getTextId() + "")) {
                            visitedStations.add(station);
                        }
                    }

                }
            }
        } catch (Exception e) {
            Log.e(FileStationGetter.class.getSimpleName(), e.toString());
        } finally {
            IOUtil.closeQuietly(fis, isr, br);
        }
        return visitedStations;
    }


}
