package com.soesec13.kinderregionsfuehrer.station;

public interface IStationDisplayer {
    void display(Station station);
}
