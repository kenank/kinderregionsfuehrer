package com.soesec13.kinderregionsfuehrer.station;

import android.content.Context;

import com.soesec13.kinderregionsfuehrer.mapHandling.Map;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface IStationGetter {
    void loadStationsFrom(Map map) throws IOException, JSONException;
    boolean hasStationInLocation(double percentageX, double percentageY);
    Station getStation(double percentageX, double percentageY);
    List<Station> getAllStations();
    Set<Station> loadVisitedStations(Context context, Set<Station> stations);
}
