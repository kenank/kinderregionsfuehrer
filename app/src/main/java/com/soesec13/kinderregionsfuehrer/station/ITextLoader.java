package com.soesec13.kinderregionsfuehrer.station;

import android.text.Spanned;

public interface ITextLoader {
    Spanned loadTextOfStation(Station station);
}
