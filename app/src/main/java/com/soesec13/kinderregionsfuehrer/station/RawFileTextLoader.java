package com.soesec13.kinderregionsfuehrer.station;

import android.content.res.Resources;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.soesec13.kinderregionsfuehrer.util.ResourceLoading;


public class RawFileTextLoader implements ITextLoader {
    private final Resources resources;
    private final Html.ImageGetter imageGetter;

    public RawFileTextLoader(Resources resources, Html.ImageGetter imageGetter) {
        this.resources = resources;
        this.imageGetter = imageGetter;
    }

    @Override
    public Spanned loadTextOfStation(Station station) {
        int textId = station.getTextId();
        String rawText;
        try {
            int rawResourceId = ResourceLoading.stationsToResourceMap.get(textId);
            rawText = ResourceLoading.getStringFromRawRes(resources, rawResourceId);
            rawText = rawText.replace("\n", "<br/>");
        } catch (Exception e)
        {
            Log.e(RawFileTextLoader.class.getSimpleName(), "Failed to load Text", e);
            rawText = "Not Available...";
        }

        return Html.fromHtml(rawText, imageGetter, null);
    }
}
