package com.soesec13.kinderregionsfuehrer.station;

import org.json.JSONException;
import org.json.JSONObject;

public class Station {
    public static final double VISIT_THRESHOLD = 350;
    private final int posX;
    private final int posY;
    private final int width;
    private final int height;
    private final String name;
    private final String gMapUri;
    private final int textId;
    private final double lat,lng;
    private final String type;

    private Station(int posX, int posY, int width, int height, String name, String gMapUri, int textId, double lat, double lng, String type) {
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
        this.name = name;
        this.gMapUri = gMapUri;
        this.textId = textId;
        this.lat = lat;
        this.lng = lng;
        this.type = type;
    }

    // If there is no gUri -> Empty Uri
    // If the json object has an empty uri -> use the lat and long
    public static Station parseJsonObject(JSONObject object) throws JSONException {
        int imageX = object.getInt("x");
        int imageY = object.getInt("y");
        int width = object.getInt("width");
        int height = object.getInt("height");
        String name = object.getString("name");
        int textId = object.getInt("textid");
        String type;
        if(object.has("type"))
        {
            type = object.getString("type");
        }
        else
        {
            type = "NA";
        }
        String gMapUri = "";
        double lat = 0;
        double lng = 0;
        if(object.has("gmaps_x") && object.has("gmaps_y")) {
            lat = object.getDouble("gmaps_x");
            lng = object.getDouble("gmaps_y");
            gMapUri = String.format("geo:%f,%f",lat,lng);
        }



        if(object.has("gMapUri"))
        {
            String uri = object.getString("gMapUri");
            if(!uri.isEmpty())
            {
                gMapUri = uri;
            }
        }

        return new Station(imageX, imageY, width, height, name, gMapUri, textId, lat, lng, type);
    }

    public boolean isClicked(int clickX, int clickY)
    {
        return clickX > posX &&
                clickX < posX + width &&
                clickY > posY &&
                clickY < posY + height;
    }

    public String getName() {
        return name;
    }

    public int getTextId() {
        return textId;
    }

    public String getGMapUri() {
        return gMapUri;
    }

    public boolean hasGeoUri(){
        return !gMapUri.isEmpty();
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Station))
        {
            return false;
        }
        Station st = (Station) obj;
        return st.getTextId() == this.getTextId();
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
