package com.soesec13.kinderregionsfuehrer.station;

public enum StationType {
    WASSER("Wasser"), WANDERN("Wandern"), WINTER("Winterspass"), SPIELPLATZ("Spielplatz"), UNKOWN("NA");
    private final String name;

    StationType(String name) {
        this.name = name;
    }

    public static StationType ofName(String name)
    {
        for(StationType type: values())
        {
            if(type.getName().equals(name))
            {
                return type;
            }
        }
        return UNKOWN;
    }

    private String getName() {
        return name;
    }
}
