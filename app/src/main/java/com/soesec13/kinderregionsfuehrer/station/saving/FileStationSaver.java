package com.soesec13.kinderregionsfuehrer.station.saving;

import android.content.Context;
import android.util.Log;

import com.soesec13.kinderregionsfuehrer.station.Station;
import com.soesec13.kinderregionsfuehrer.station.tracking.IStationVisitor;
import com.soesec13.kinderregionsfuehrer.util.IOUtil;

import java.io.FileOutputStream;

/**
 * Created by Sebi on 07/01/2018.
 */

public class FileStationSaver implements IStationSaver, IStationVisitor {
    public static final String FILE_STATIONS_VISITED = "visited_stations.txt";
    private final Context context;

    public FileStationSaver(Context context) {
        this.context = context;
    }

    @Override
    public void saveVisitedStation(Station station) {
        FileOutputStream outputStream = null;
        try {
            outputStream = context.openFileOutput(FILE_STATIONS_VISITED, Context.MODE_PRIVATE);
            byte[] output = (""+station.getTextId()).getBytes();
            outputStream.write(output);
            outputStream.close();
        } catch (Exception e) {
            Log.e("AchievementManager", "Visit of station could not be saved:\n" + e.toString());
        } finally {
            IOUtil.closeQuietly(outputStream);
        }
    }

    @Override
    public void visitStation(Station station) {
        try {
            this.saveVisitedStation(station);
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Failed to save Station");
        }
    }
}
