package com.soesec13.kinderregionsfuehrer.station.saving;

import com.soesec13.kinderregionsfuehrer.station.Station;

/**
 * Created by Sebi on 07/01/2018.
 */

public interface IStationSaver {
    void saveVisitedStation(Station station);
}
