package com.soesec13.kinderregionsfuehrer.station.tracking;

import com.soesec13.kinderregionsfuehrer.station.Station;

/**
 * Created by Sebi on 04/01/2018.
 */

public interface IVisitationChecker {
    boolean checkIsVisited(Station station);
}
