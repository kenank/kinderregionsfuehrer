package com.soesec13.kinderregionsfuehrer.station.tracking;

import com.soesec13.kinderregionsfuehrer.station.Station;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebi on 04/01/2018.
 */

public class StationTracker {
    private final List<IStationVisitor> visitorList = new ArrayList<>();
    private final List<IVisitationChecker> checkerList = new ArrayList<>();

    public void notifyVisit(Station station)
    {
        for(IStationVisitor visitor: visitorList)
        {
            visitor.visitStation(station);
        }
    }

    public boolean isStationVisited(Station station)
    {
        for(IVisitationChecker checker: checkerList)
        {
            if(checker.checkIsVisited(station))
            {
                return true;
            }
        }
        return false;
    }

    public void addVisitor(IStationVisitor visitor)
    {
        visitorList.add(visitor);
    }

    public void addVisitationChecker(IVisitationChecker checker)
    {
        checkerList.add(checker);
    }
}
