package com.soesec13.kinderregionsfuehrer.util;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;

/**
 * Created by Sebi on 21/11/2017.
 */

public class DialogUtil {
    public static void displayWarning(Context context, String title, String message)
    {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
