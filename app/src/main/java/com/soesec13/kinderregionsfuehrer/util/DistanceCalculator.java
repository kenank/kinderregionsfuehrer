package com.soesec13.kinderregionsfuehrer.util;

/**
 * Created by Sebi on 04/01/2018.
 */

public class DistanceCalculator {
    /**
     * Returns the distance between two coordinates in meters
     * @param lat1 Latitude of first coordinate
     * @param long1 Longitude of first coordinate
     * @param lat2 Latitude of second coordinate
     * @param long2 Longitude of second coordinate
     * @return Distance in meters
     */
    public static double distance(double lat1, double long1, double lat2, double long2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(long2 - long1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c * 1000;
    }
}
