package com.soesec13.kinderregionsfuehrer.util;

import android.util.Log;

import java.io.Closeable;
import java.io.IOException;

public class IOUtil {

    public static void closeQuietly(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException ex) {
                    Log.e(IOUtil.class.getSimpleName(), "Could not close stream:\n" + ex.getMessage());
                }
            }
        }
    }
}
