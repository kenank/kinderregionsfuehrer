package com.soesec13.kinderregionsfuehrer.util;

import android.content.res.Resources;
import android.util.Log;

import com.soesec13.kinderregionsfuehrer.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sebi on 27/08/2017.
 */

public class ResourceLoading {
    public static String getStringFromRawRes(Resources resources, int rawRes) {
        /*InputStream in_s = resources.openRawResource(rawRes);
        byte[] b = new byte[in_s.available()];
        in_s.read(b);
        return new String(b);*/
        String line = null;
        InputStream in_s = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;

        try {
            in_s = resources.openRawResource(rawRes);
            inputStreamReader = new InputStreamReader(in_s, "ISO-8859-15");
            bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("<br/>");
            }
            in_s.close();
            line = stringBuilder.toString();

            bufferedReader.close();
        } catch (IOException ex) {
            Log.d(ResourceLoading.class.getSimpleName(), ex.toString());
        } finally {
            IOUtil.closeQuietly(in_s, inputStreamReader, bufferedReader);
        }
        return line;
    }

    public static final Map<Integer, Integer> stationsToResourceMap;
    static{
        stationsToResourceMap = new HashMap<>();
        stationsToResourceMap.put(1, R.raw.eislaufen_nikolaiberg);
        stationsToResourceMap.put(2,R.raw.spielplatz_skaterplatz_sausal);
        stationsToResourceMap.put(3,R.raw.tillmitscher_rundwanderweg);
        stationsToResourceMap.put(4,R.raw.planschen_lassnitz);
        stationsToResourceMap.put(5,R.raw.spielplatz_tillmitsch);
        stationsToResourceMap.put(6,R.raw.eislaufen_tillmitsch);
        stationsToResourceMap.put(7,R.raw.spielplatz_neutillmitsch);
        stationsToResourceMap.put(8,R.raw.naturparkzentrum_grottenhof);
        stationsToResourceMap.put(9,R.raw.sulmbad_steinern);
        stationsToResourceMap.put(10,R.raw.kaindorf_spielplatz);
        stationsToResourceMap.put(11,R.raw.eislaufen_marenzipark);
        stationsToResourceMap.put(12,R.raw.spielplatz_marenzipark);
        stationsToResourceMap.put(13,R.raw.seggauberger_genussplatzrunde);
        stationsToResourceMap.put(14,R.raw.spielplatz_jufa);
        stationsToResourceMap.put(15,R.raw.freibad_leibnitz);
        stationsToResourceMap.put(16,R.raw.freizeitparadies_sulmsee);
        stationsToResourceMap.put(17,R.raw.neuer_spielplatz);
        stationsToResourceMap.put(18,R.raw.kitzecker_weinwanderweg);
        stationsToResourceMap.put(19,R.raw.schloss_harrachegg);
        stationsToResourceMap.put(20,R.raw.naturerlebnis_demmerkogel);
        stationsToResourceMap.put(21,R.raw.eislaufen_eisstockschiessen);
        stationsToResourceMap.put(22,R.raw.generationenplatz_spielplatz);
        stationsToResourceMap.put(23,R.raw.spielplatz_kindergarten);
        stationsToResourceMap.put(24,R.raw.naturbadesee_gleinstaetten);
        stationsToResourceMap.put(25,R.raw.eislaufen_badesee);
        stationsToResourceMap.put(26,R.raw.landartpark_aupark);
        stationsToResourceMap.put(27,R.raw.gleinstaettner_runde);
        stationsToResourceMap.put(28,R.raw.schotterteich_aldrian);
        stationsToResourceMap.put(29,R.raw.bobfahren_grossklein);
        stationsToResourceMap.put(30,R.raw.eislaufflaeche_grossklein);
        stationsToResourceMap.put(31,R.raw.spielplatz_skaterplatz);
        stationsToResourceMap.put(32,R.raw.urgeschichtlicher_wanderweg);
        stationsToResourceMap.put(33,R.raw.heimschuher_runde);
        stationsToResourceMap.put(34,R.raw.heimschuh_generationenspielplatz);
        stationsToResourceMap.put(35,R.raw.auen_moorwanderweg);
        stationsToResourceMap.put(36,R.raw.spielfeld_eislaufen);
        stationsToResourceMap.put(37,R.raw.spielplatz_kasernenstrasse);
        stationsToResourceMap.put(38,R.raw.spielplatz_volksschule);
        stationsToResourceMap.put(39,R.raw.rebenwanderweg);
        stationsToResourceMap.put(40,R.raw.freizeitzentrum_freibad);
        stationsToResourceMap.put(41,R.raw.bergbad_retznei);
        stationsToResourceMap.put(42,R.raw.motorikpark_gamlitz);
        stationsToResourceMap.put(43,R.raw.spielplatz_gamlitz);
        stationsToResourceMap.put(44,R.raw.vitalwanderweg);
        stationsToResourceMap.put(45,R.raw.spielplatz_freibad);
        stationsToResourceMap.put(46,R.raw.freibad_leutschach);
        stationsToResourceMap.put(47,R.raw.minigolf_tour);
        stationsToResourceMap.put(48,R.raw.heiligengeistklamm);
        stationsToResourceMap.put(49,R.raw.remschniggalm);
        stationsToResourceMap.put(50,R.raw.eislaufen_freibad);
        stationsToResourceMap.put(51,R.raw.freibad_arnfels);
        stationsToResourceMap.put(52,R.raw.spielplatz_musikheim);
        stationsToResourceMap.put(53,R.raw.schlossberg_eichberg);
        stationsToResourceMap.put(54,R.raw.weg_der_sinne);
        stationsToResourceMap.put(55,R.raw.napukis_geheimnis);
        stationsToResourceMap.put(56,R.raw.bobfahren_oberhaag);
        stationsToResourceMap.put(57,R.raw.loamtradl);
        stationsToResourceMap.put(58,R.raw.eislaufplatz_beachvolleyball);
        stationsToResourceMap.put(59,R.raw.naturbadeteich_oberhaag);
        stationsToResourceMap.put(60,R.raw.blumenreichweg);
        stationsToResourceMap.put(61,R.raw.geowanderweg);
        stationsToResourceMap.put(62,R.raw.altenbachklamm);
        stationsToResourceMap.put(63, R.raw.kapellenweg);
    }
}
