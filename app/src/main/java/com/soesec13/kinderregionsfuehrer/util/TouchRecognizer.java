package com.soesec13.kinderregionsfuehrer.util;

import android.content.res.Resources;
import android.view.MotionEvent;

/**
 * Created by Sebi on 27/08/2017.
 */

public class TouchRecognizer {
    private final Resources resources;
    private static final int MAX_CLICK_DURATION = 1000;
    private static final int MAX_CLICK_DISTANCE = 15;

    private long pressStartTime;
    private float pressedX;
    private float pressedY;

    private boolean isTouchEvent;
    private boolean stayedWithinClickDistance;

    public TouchRecognizer(Resources resources) {
        this.resources = resources;
    }

    public void onMotionEvent(MotionEvent e)
    {
        isTouchEvent = false;
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                pressStartTime = System.currentTimeMillis();
                pressedX = e.getX();
                pressedY = e.getY();
                stayedWithinClickDistance = true;
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (stayedWithinClickDistance && distance(pressedX, pressedY, e.getX(), e.getY()) > MAX_CLICK_DISTANCE) {
                    stayedWithinClickDistance = false;
                }
                break;
            }
            case MotionEvent.ACTION_UP: {
                long pressDuration = System.currentTimeMillis() - pressStartTime;
                if (pressDuration < MAX_CLICK_DURATION && stayedWithinClickDistance) {
                    isTouchEvent = true;
                }
            }
        }
    }

    public boolean isTouchEvent() {
        return isTouchEvent;
    }

    private float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private float pxToDp(float px) {
        return px / resources.getDisplayMetrics().density;
    }
}
