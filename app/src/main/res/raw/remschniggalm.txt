Start: Arnfels Hauptplatz � Johannisbr�cke
Dauer: etwa 3 Stunden f�r den gesamten Rundweg
Ausr�stung: gutes Schuhwerk
�ber das Windische Wegerl, zur Salzerkapelle, auf den Salzerkogel und weiter auf den Remschnigg. Macht man einen kleinen Abstecher, kommt man leicht zur Almh�tte auf der Remschniggalm. Die Wanderung bietet Ausblicke in die umgebende Landschaft. Besonders auf der Remschniggalm selbst, der s�dlichsten Alm �sterreichs.